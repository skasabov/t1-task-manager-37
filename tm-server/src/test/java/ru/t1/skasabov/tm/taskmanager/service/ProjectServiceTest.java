package ru.t1.skasabov.tm.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.service.*;
import ru.t1.skasabov.tm.enumerated.ProjectSort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.service.*;

import java.util.*;

public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    private static boolean isBackup = false;

    @NotNull
    private List<Project> projects;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @Before
    public void initTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        userService = new UserService(connectionService, propertyService);
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
        projects = projectService.findAll();
        tasks = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            tasks.addAll(taskService.findAllByProjectId(project.getUserId(), project.getId()));
        }
        USER_ID_ONE = userService.create("user_one", "user_one").getId();
        USER_ID_TWO = userService.create("user_two", "user_two").getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test " + i);
            if (i <= 5) project.setUserId(USER_ID_ONE);
            else project.setUserId(USER_ID_TWO);
            projectService.add(project);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateForEmptyUser() {
        projectService.create("", "test", "");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateEmptyName() {
        projectService.create(USER_ID_ONE, "", "");
    }

    @Test
    public void testCreateName() {
        final int expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final String name = "Test Project";
        @NotNull final Project actualProject = projectService.create(USER_ID_ONE, name, "");
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        Assert.assertEquals(USER_ID_ONE, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
    }

    @Test
    public void testCreateDescription() {
        final int expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        @NotNull final Project actualProject = projectService.create(USER_ID_TWO, name, description);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        Assert.assertEquals(USER_ID_TWO, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void createProject() {
        final int expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        @NotNull final Date dateBegin = new Date();
        @NotNull final Date dateEnd = new Date();
        @NotNull final Project actualProject = projectService.create(
                USER_ID_ONE, name, description, dateBegin, dateEnd
        );
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        Assert.assertEquals(USER_ID_ONE, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
        Assert.assertEquals(dateBegin, actualProject.getDateBegin());
        Assert.assertEquals(dateEnd, actualProject.getDateEnd());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        @NotNull final Project project = projectService.updateById(USER_ID_TWO, id, name, description);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_ID_TWO, project.getUserId());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByEmptyId() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById(USER_ID_TWO, "", name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdForEmptyUser() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById("", id, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithEmptyName() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String description = "Test Description One";
        projectService.updateById(USER_ID_TWO, id, "", description);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateByIdProjectNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById(USER_ID_TWO, id, name, description);
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        @NotNull final Project project = projectService.updateByIndex(USER_ID_ONE, 1, name, description);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_ID_ONE, project.getUserId());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByEmptyIndex() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(USER_ID_ONE, null, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexForEmptyUser() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex("", 1, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithEmptyName() {
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(USER_ID_ONE, 1, "", description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByNegativeIndex() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(USER_ID_ONE, -2, name, description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIncorrectIndex() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(USER_ID_ONE, 5, name, description);
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final Project project = projectService.changeProjectStatusById(USER_ID_TWO, id, status);
        Assert.assertEquals(USER_ID_TWO, project.getUserId());
        Assert.assertEquals(status, project.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeProjectStatusByEmptyId() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById(USER_ID_TWO, "", status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusByIdForEmptyUser() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById("", id, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeProjectStatusByIdWithEmptyStatus() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        projectService.changeProjectStatusById(USER_ID_TWO, id, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeProjectStatusByIdWithIncorrectStatus() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        projectService.changeProjectStatusById(USER_ID_TWO, id, Status.toStatus("123"));
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeProjectStatusByIdProjectNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById(USER_ID_TWO, id, status);
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final Project project = projectService.changeProjectStatusByIndex(USER_ID_ONE, 1, status);
        Assert.assertEquals(USER_ID_ONE, project.getUserId());
        Assert.assertEquals(status, project.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByEmptyIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex(USER_ID_ONE, null, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusByIndexForEmptyUser() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex("", 1, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeProjectStatusByIndexWithEmptyStatus() {
        projectService.changeProjectStatusByIndex(USER_ID_ONE, 1, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeProjectStatusByIndexWithIncorrectStatus() {
        projectService.changeProjectStatusByIndex(USER_ID_ONE, 1, Status.toStatus("123"));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByIncorrectIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex(USER_ID_ONE, 5, status);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByNegativeIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex(USER_ID_ONE, -2, status);
    }

    @Test
    public void testAdd() {
        final int expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final Project project = new Project();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectService.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNull() {
        projectService.add(null);
    }

    @Test
    public void testAddForUser() {
        final int expectedNumberOfEntries = projectService.getSize(USER_ID_TWO) + 1;
        @NotNull final Project project = new Project();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectService.add(USER_ID_TWO, project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(USER_ID_TWO));
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNullForUser() {
        projectService.add(USER_ID_TWO, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testAddForEmptyUser() {
        @NotNull final Project project = new Project();
        project.setUserId(USER_ID_TWO);
        projectService.add("", project);
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = projectService.getSize() + 4;
        @NotNull final List<Project> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test Project " + i);
            project.setUserId(USER_ID_ONE);
            actualProjects.add(project);
        }
        projectService.addAll(actualProjects);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testAddAllNull() {
        final int expectedNumberOfEntries = projectService.getSize();
        projectService.addAll(null);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testSet() {
        isBackup = true;
        @NotNull final List<Project> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test Project " + i);
            project.setUserId(USER_ID_TWO);
            actualProjects.add(project);
        }
        projectService.set(actualProjects);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
    }

    @Test
    public void testSetNull() {
        final int expectedNumberOfEntries = projectService.getSize();
        projectService.set(null);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testClearAll() {
        isBackup = true;
        projectService.removeAll();
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final int expectedNumberOfEntries = projectService.getSize() - NUMBER_OF_ENTRIES / 2;
        projectService.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = projectService.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Project> projectList = projectService.findAll(USER_ID_TWO);
        projectService.removeAll(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projectList = projectService.findAll();
        Assert.assertEquals(projectList.size(), projectService.getSize());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final List<Project> projectList = projectService.findAll();
        @NotNull final List<Project> projectSortList = projectService.findAll(sort.getComparator());
        Assert.assertNotEquals(projectList, projectSortList);
    }

    @Test
    public void testFindAllWithEmptyComparator() {
        @NotNull final List<Project> projectList = projectService.findAll();
        @NotNull final List<Project> projectSortList = projectService.findAll((Comparator<Project>) null);
        Assert.assertEquals(projectList, projectSortList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> projectList = projectService.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectList.size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForEmptyUser() {
        projectService.findAll("");
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final List<Project> projectList = projectService.findAll(USER_ID_TWO);
        @NotNull final List<Project> projectSortList = projectService.findAll(USER_ID_TWO, sort.getComparator());
        Assert.assertNotEquals(projectList, projectSortList);
    }

    @Test
    public void testFindAllWithEmptyComparatorForUser() {
        @NotNull final List<Project> projectList = projectService.findAll(USER_ID_TWO);
        @NotNull final List<Project> projectSortList = projectService.findAll(USER_ID_TWO, null);
        Assert.assertEquals(projectList, projectSortList);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllWithComparatorForEmptyUser() {
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        projectService.findAll("", sort.getComparator());
    }

    @Test
    public void testFindById() {
        @Nullable final Project project = projectService.findAll().get(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        @Nullable final Project actualProject = projectService.findOneById(projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyId() {
        Assert.assertNull(projectService.findOneById(""));
    }

    @Test
    public void testFindByIdForUser() {
        @Nullable final Project project = projectService.findAll(USER_ID_ONE).get(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = projectService.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final Project actualProject = projectService.findOneById(USER_ID_ONE, projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyIdForUser() {
        Assert.assertNull(projectService.findOneById(USER_ID_ONE, ""));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIdForEmptyUser() {
        @NotNull final String projectId = projectService.findAll(USER_ID_ONE).get(0).getId();
        projectService.findOneById("", projectId);
    }

    @Test
    public void testFindByIdProjectNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectService.findOneById(id));
    }

    @Test
    public void testFindByIdProjectNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectService.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Project project = projectService.findAll().get(0);
        @Nullable final Project actualProject = projectService.findOneByIndex(1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndex() {
        projectService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndex() {
        projectService.findOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndex() {
        projectService.findOneByIndex(projectService.getSize() + 1);
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Project project = projectService.findAll(USER_ID_TWO).get(0);
        @Nullable final Project actualProject = projectService.findOneByIndex(USER_ID_TWO, 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndexForUser() {
        projectService.findOneByIndex(USER_ID_TWO, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndexForUser() {
        projectService.findOneByIndex(USER_ID_TWO, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndexForUser() {
        projectService.findOneByIndex(USER_ID_TWO, 12);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIndexForEmptyUser() {
        projectService.findOneByIndex("", 0);
    }

    @Test
    public void testGetSize() {
        final int expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final Project project = new Project();
        project.setName("Test Project");
        project.setUserId(USER_ID_ONE);
        projectService.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final Project project = new Project();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectService.add(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectService.getSize(USER_ID_TWO));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForEmptyUser() {
        projectService.getSize("");
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = projectService.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectService.existsById(invalidId));
        Assert.assertTrue(projectService.existsById(validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyId() {
        projectService.existsById("");
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = projectService.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectService.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(projectService.existsById(USER_ID_ONE, validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyIdForUser() {
        projectService.existsById(USER_ID_ONE, "");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testIsNotFoundForEmptyUser() {
        projectService.existsById("", projectService.findAll(USER_ID_ONE).get(0).getId());
    }

    @Test
    public void testRemove() {
        isBackup = true;
        final int expectedNumberOfEntries = projectService.getSize() - 1;
        @NotNull final Project project = projectService.findAll().get(0);
        projectService.removeOne(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNull() {
        projectService.removeOne(null);
    }

    @Test
    public void testRemoveForUser() {
        final int expectedNumberOfEntries = projectService.getSize(USER_ID_TWO) - 1;
        @NotNull final Project project = projectService.findAll(USER_ID_TWO).get(0);
        projectService.removeOne(USER_ID_TWO, project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(USER_ID_TWO));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveForEmptyUser() {
        @NotNull final Project project = projectService.findAll().get(0);
        projectService.removeOne("", project);
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNullForUser() {
        projectService.removeOne(USER_ID_TWO, null);
    }

    @Test
    public void testRemoveById() {
        isBackup = true;
        final int expectedNumberOfEntries = projectService.getSize() - 1;
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        projectService.removeOneById(projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = projectService.getSize(USER_ID_ONE) - 1;
        @NotNull final String projectId = projectService.findAll(USER_ID_ONE).get(0).getId();
        projectService.removeOneById(USER_ID_ONE, projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(USER_ID_ONE));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyId() {
        Assert.assertNull(projectService.removeOneById(""));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyIdForUser() {
        projectService.removeOneById(USER_ID_ONE, "");
    }

    @Test
    public void testRemoveByIdProjectNotFound() {
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertNull(projectService.removeOneById(invalidId));
    }

    @Test
    public void testRemoveByIdProjectNotFoundForUser() {
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertNull(projectService.removeOneById(USER_ID_ONE, invalidId));
    }

    @Test
    public void testRemoveByIndex() {
        isBackup = true;
        final int expectedNumberOfEntries = projectService.getSize() - 1;
        projectService.removeOneByIndex(1);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndex() {
        projectService.removeOneByIndex(null);
    }

    @Test
    public void testRemoveByIndexForUser() {
        final int expectedNumberOfEntries = projectService.getSize(USER_ID_TWO) - 1;
        projectService.removeOneByIndex(USER_ID_TWO, 1);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(USER_ID_TWO));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndexForUser() {
        projectService.removeOneByIndex(USER_ID_TWO, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIndexForEmptyUser() {
        projectService.removeOneByIndex("", 1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndex() {
        projectService.removeOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndexForUser() {
        projectService.removeOneByIndex(USER_ID_TWO, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndex() {
        projectService.removeOneByIndex(projectService.getSize() + 1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndexForUser() {
        projectService.removeOneByIndex(USER_ID_TWO, 5);
    }

    @After
    public void clearRepository() {
        if (isBackup) {
            projectService.set(projects);
            taskService.addAll(tasks);
        }
        projectService.removeAll(USER_ID_ONE);
        projectService.removeAll(USER_ID_TWO);
        userService.removeOneById(USER_ID_ONE);
        userService.removeOneById(USER_ID_TWO);
    }

}
