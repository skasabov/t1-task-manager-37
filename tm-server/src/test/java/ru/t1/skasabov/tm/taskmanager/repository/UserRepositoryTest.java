package ru.t1.skasabov.tm.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.api.service.*;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.UserRepository;
import ru.t1.skasabov.tm.service.*;
import ru.t1.skasabov.tm.util.HashUtil;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private User cat;

    @NotNull
    private User mouse;
    
    @NotNull
    private Connection connection;

    @NotNull
    private List<User> users;

    @NotNull
    private List<Project> projects;

    @NotNull
    private List<Session> sessions;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private IUserService userService;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private IUserRepository userRepository;

    @Before
    public void initRepository() {
        propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        userService = new UserService(connectionService, propertyService);
        projectService = new ProjectService(connectionService);
        sessionService = new SessionService(connectionService);
        taskService = new TaskService(connectionService);
        userRepository = new UserRepository(connection);
        users = userService.findAll();
        projects = new ArrayList<>();
        sessions = new ArrayList<>();
        tasks = new ArrayList<>();
        for (@NotNull final User user : users) {
            projects.addAll(projectService.findAll(user.getId()));
            sessions.addAll(sessionService.findAll(user.getId()));
            tasks.addAll(taskService.findAll(user.getId()));
        }
        cat = new User();
        cat.setLogin("cat");
        cat.setPasswordHash(HashUtil.salt(propertyService, "cat"));
        cat.setEmail("cat@cat");
        mouse = new User();
        mouse.setLogin("mouse");
        mouse.setPasswordHash(HashUtil.salt(propertyService, "mouse"));
        mouse.setEmail("mouse@mouse");
        userRepository.add(cat);
        userRepository.add(mouse);
    }

    @Test
    public void testUpdate() {
        mouse.setLastName("mouse");
        mouse.setFirstName("mouse");
        mouse.setMiddleName("mouse");
        @NotNull final User actualUser = userRepository.update(mouse);
        Assert.assertEquals("mouse", actualUser.getLastName());
        Assert.assertEquals("mouse", actualUser.getFirstName());
        Assert.assertEquals("mouse", actualUser.getMiddleName());
    }

    @Test
    public void testAdd() {
        final int expectedUsers = userRepository.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("dog");
        user.setPasswordHash(HashUtil.salt(propertyService, "dog"));
        userRepository.add(user);
        Assert.assertEquals(expectedUsers, userRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = userRepository.getSize() + 4;
        @NotNull final List<User> actualUsers = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final User user = new User();
            user.setLogin("user " + i);
            user.setPasswordHash(HashUtil.salt(propertyService, "user " + i));
            actualUsers.add(user);
        }
        userRepository.addAll(actualUsers);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<User> actualUsers = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user " + i);
            user.setPasswordHash(HashUtil.salt(propertyService, "user " + i));
            actualUsers.add(user);
        }
        userRepository.set(actualUsers);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testCreate() {
        final int expectedNumberOfEntries = userRepository.getSize() + 1;
        userRepository.create("dog", "dog", "dog@dog");
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testCreateRole() {
        final int expectedNumberOfEntries = userRepository.getSize() + 1;
        userRepository.create("dog", "dog", Role.USUAL);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testClearAll() {
        userRepository.removeAll();
        Assert.assertEquals(0, userRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = userRepository.getSize() - 2;
        @NotNull final List<User> userList = new ArrayList<>();
        userList.add(cat);
        userList.add(mouse);
        userRepository.removeAll(userList);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> userList = userRepository.findAll();
        Assert.assertEquals(userList.size(), userRepository.getSize());
    }

    @Test
    public void testFindById() {
        @Nullable final User actualUser = userRepository.findOneById(cat.getId());
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByIdUserNotFound() {
        Assert.assertNull(userRepository.findOneById("some_id"));
    }

    @Test
    public void testFindByLogin() {
        @Nullable final User actualUser = userRepository.findByLogin("cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByLoginUserNotFound() {
        Assert.assertNull(userRepository.findByLogin("dog"));
    }

    @Test
    public void testFindByEmail() {
        @Nullable final User actualUser = userRepository.findByEmail("cat@cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByEmailUserNotFound() {
        Assert.assertNull(userRepository.findByEmail("dog@dog"));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final User user = userRepository.findAll().get(0);
        @Nullable final User actualUser = userRepository.findOneByIndex(1);
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(user.getLogin(), actualUser.getLogin());
        Assert.assertEquals(user.getEmail(), actualUser.getEmail());
        Assert.assertEquals(user.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(user.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByIndexUserNotFound() {
        userRepository.removeAll();
        Assert.assertNull(userRepository.findOneByIndex(1));
    }

    @Test
    public void testGetSize() {
        final int expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("dog");
        user.setPasswordHash(HashUtil.salt(propertyService, "dog"));
        user.setEmail("dog@dog");
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = userRepository.findAll().get(0).getId();
        @NotNull final String invalidId = "some_id";
        Assert.assertFalse(userRepository.existsById(invalidId));
        Assert.assertTrue(userRepository.existsById(validId));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = userRepository.getSize() - 1;
        userRepository.removeOne(mouse);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = userRepository.getSize() - 1;
        userRepository.removeOneById(mouse.getId());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = userRepository.getSize() - 1;
        userRepository.removeOneByIndex(1);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testRemoveByIndexUserNotFound() {
        userRepository.removeAll();
        Assert.assertNull(userRepository.removeOneByIndex(1));
    }

    @Test
    public void testLoginExist() {
        Assert.assertTrue(userRepository.isLoginExist("cat"));
    }

    @Test
    public void testEmptyLoginExist() {
        Assert.assertFalse(userRepository.isLoginExist(""));
    }

    @Test
    public void testEmailExist() {
        Assert.assertTrue(userRepository.isEmailExist("cat@cat"));
    }

    @Test
    public void testEmptyEmailExist() {
        Assert.assertFalse(userRepository.isEmailExist(""));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        connection.commit();
        userService.set(users);
        projectService.addAll(projects);
        sessionService.addAll(sessions);
        taskService.addAll(tasks);
        connection.close();
    }

}
