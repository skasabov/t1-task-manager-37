package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
