package ru.t1.skasabov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @NotNull Date dateBegin,
            @NotNull Date dateEnd
    );

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    Task update(@NotNull Task task);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
