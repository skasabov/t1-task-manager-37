package ru.t1.skasabov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.IUserOwnedRepository;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.IUserOwnedService;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.field.IdEmptyException;
import ru.t1.skasabov.tm.exception.field.IndexIncorrectException;
import ru.t1.skasabov.tm.exception.field.UserIdEmptyException;
import ru.t1.skasabov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.removeAll(userId);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.existsById(userId, id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId, comparator);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findOneById(userId, id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findOneByIndex(userId, index);
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try(@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.getSize(userId);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        M model;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            model = repository.removeOneById(userId, id);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @NotNull final Connection connection = getConnection();
        M model;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            model = repository.removeOneByIndex(userId, index);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.add(userId, model);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.removeOne(userId, model);
            connection.commit();
        }
        catch(@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

}
