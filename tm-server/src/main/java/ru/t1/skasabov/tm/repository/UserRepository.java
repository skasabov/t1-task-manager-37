package ru.t1.skasabov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.DBConstants;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @NotNull final String login,
            @NotNull final String passwordHash
    ) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String email
    ) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final Role role
    ) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        return add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?::role_type WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_LOGIN, DBConstants.COLUMN_EMAIL,
                DBConstants.COLUMN_PASSWORD_HASH, DBConstants.COLUMN_LAST_NAME,
                DBConstants.COLUMN_FIRST_NAME, DBConstants.COLUMN_MIDDLE_NAME,
                DBConstants.COLUMN_LOCKED, DBConstants.COLUMN_ROLE,
                DBConstants.COLUMN_ID);
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getMiddleName());
            statement.setBoolean(7, user.getLocked());
            statement.setString(8, user.getRole().toString());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                DBConstants.COLUMN_LOGIN
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                DBConstants.COLUMN_EMAIL
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@NotNull final String login) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                DBConstants.COLUMN_LOGIN
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@NotNull final String email) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                DBConstants.COLUMN_EMAIL
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        }
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstants.TABLE_USER;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString(DBConstants.COLUMN_ID));
        user.setLogin(row.getString(DBConstants.COLUMN_LOGIN));
        user.setEmail(row.getString(DBConstants.COLUMN_EMAIL));
        user.setLastName(row.getString(DBConstants.COLUMN_LAST_NAME));
        user.setFirstName(row.getString(DBConstants.COLUMN_FIRST_NAME));
        user.setMiddleName(row.getString(DBConstants.COLUMN_MIDDLE_NAME));
        user.setPasswordHash(row.getString(DBConstants.COLUMN_PASSWORD_HASH));
        user.setLocked(row.getBoolean(DBConstants.COLUMN_LOCKED));
        user.setRole(Role.toRole(row.getString(DBConstants.COLUMN_ROLE)));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?::role_type)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_LOGIN, DBConstants.COLUMN_EMAIL,
                DBConstants.COLUMN_LAST_NAME, DBConstants.COLUMN_FIRST_NAME, DBConstants.COLUMN_MIDDLE_NAME,
                DBConstants.COLUMN_PASSWORD_HASH, DBConstants.COLUMN_LOCKED, DBConstants.COLUMN_ROLE
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getPasswordHash());
            statement.setBoolean(8, user.getLocked());
            statement.setString(9, user.getRole().toString());
            statement.executeUpdate();
        }
        return user;
    }

}
