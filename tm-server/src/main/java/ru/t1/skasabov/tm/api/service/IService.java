package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(@Nullable M model);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @NotNull
    Collection<M> addAll(@Nullable Collection<M> models);

    @NotNull
    Collection<M> set(@Nullable Collection<M> models);

    @NotNull
    Boolean existsById(@Nullable String id);

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    int getSize();

    @Nullable
    M removeOne(@Nullable M model);

    @Nullable
    M removeOneById(@Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<M> collection);

    void removeAll();

}
