package ru.t1.skasabov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.User;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(@Nullable final User user) {
        super(user);
    }

}
