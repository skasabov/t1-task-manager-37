package ru.t1.skasabov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.exception.user.RoleEmptyException;
import ru.t1.skasabov.tm.exception.user.RoleIncorrectException;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) throw new RoleEmptyException();
        for (@NotNull final Role role : values()) {
            if (role.name().equals(value)) return role;
        }
        throw new RoleIncorrectException();
    }

}
