package ru.t1.skasabov.tm.exception.user;

public final class RoleIncorrectException extends AbstractUserException {

    public RoleIncorrectException() {
        super("Error! Role is incorrect...");
    }

}
