package ru.t1.skasabov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectClearResponse extends AbstractResponse {

    @NotNull
    private List<Project> projects = new ArrayList<>();

    public ProjectClearResponse(@NotNull final List<Project> projects) {
        this.projects = projects;
    }

}
